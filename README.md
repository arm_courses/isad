# 《信息系统分析与设计》_"Information System Analysis and Design"_

## 许可协议

![CC-BY-SA-4.0](./resources/image/CC_BY-SA_88x31.png)

采用`CC-BY-SA-4.0`协议，署名：aRoming。

## 参考文献Bibliographies

### 书籍Books

1. :book: 《系统分析与设计（原书第10版）》_"Systems Analysis & Design(10th Edition)"_. Kenneth E. Kendall, Julie E. Kendall（著）. 文家焱，施平安（译）. 机械工业出版社. ISBN: 9787111635253. 2019-12-04. [豆瓣链接](https://book.douban.com/subject/34920713/), [京东链接](https://item.jd.com/12778450.html)
    1. :desktop_computer: [Books by Kenneth E. Kendall and Julie E. Kendall](https://media.pearsoncmg.com/ph/bp/bridgepages/teamsite/kendall/)
1. :book: 《信息系统分析与设计（第4版）》，王晓敏 邝孔武 著，清华大学出版社，2013-08，ISBN：9787302329824
1. :book: [《软件需求》（第3版）](http://www.tup.tsinghua.edu.cn/booksCenter/book_05387201.html)，[美]Karl Wiegers, Joy Beatty著，李忠利 李淳 霍金健 孔晨辉译，清华大学出版社，2016-02，ISBN：9787302426820，[豆瓣链接](https://book.douban.com/subject/26307910/)
1. :book: 《软件需求分析师岗位指导教程》，桂超主编、孔敏丛书主编，南京大学出版社，2017-01，ISBN：9787305145155
1. :book: 《大象：Thinking in UML（第2版）》，谭云杰 著，中国水利水电出版社，2012-03，ISBN：9787508492346
    1. :computer: [大象-Thinking in UML（第二版）-配套资料-谭云杰（2012-03-14）](http://waterpub.com.cn/softdown/FileInfo-1.Asp?SoftID=3584)
    1. :computer: [大象-Thinking in UML（第二版）](http://www.wsbookshow.com/bookshow/jc/yjs/qtl/11383.html)
    1. :computer: [coffeewoo的专栏：Thinking in UML](https://blog.csdn.net/coffeewoo)
    1. :computer: [coffeewoo itpub](http://coffeewoo.itpub.net/)（可能已失效）

### 其他Others

1. :computer: PeterDon《信息系统分析与设计》笔记
    1. [《信息系统分析与设计》笔记No.1](https://www.cnblogs.com/PeterDon-WorkHardPlayHard/p/12556222.html)
    1. [《信息系统分析与设计》笔记No.2](https://www.cnblogs.com/PeterDon-WorkHardPlayHard/p/12571411.html)
    1. [《信息系统分析与设计》笔记No.3](https://www.cnblogs.com/PeterDon-WorkHardPlayHard/p/12578703.html)
    1. [《信息系统分析与设计》笔记No.4](https://www.cnblogs.com/PeterDon-WorkHardPlayHard/p/12594428.html)
    1. [《信息系统分析与设计》笔记No.5](https://www.cnblogs.com/PeterDon-WorkHardPlayHard/p/12596056.html)
