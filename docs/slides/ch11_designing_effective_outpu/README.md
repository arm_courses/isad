---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Designing Effective Output_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Designing Effective Output设计有效的输出

---

## Contents

1. 输出设计的目标
1. 输出内容与输出方式相关联
1. 根据媒介设计输出：打印输出、屏幕输出
1. 设计web输出
1. 设计app输出
1. 社交媒体设计

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 输出设计的目标

---

1. 设计符合预定目标的输出
1. 设计符合用户习惯的输出
1. 设计合适数量的输出
1. 确保输出的信息是有用的
1. 确保输出的及时性
1. 确保输出方式的有效性

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 输出内容与输出方式相关联

---

### 输出方式

---

![width:840](./resources/image/fig11_01.jpg)

---

### 选择输出方式应考虑的因素

1. 输出给谁？
1. 输出给多少人？
1. 输出的目的是什么？
1. 输出的频次有多高？
1. 输出的响应速度要多快？
1. 输出需保留存储多长时间？
1. 产生、存储和输出须遵循的规范是什么？
1. 维护和供应的成本是多少？
1. 对使用场景的要求是什么？

---

<!--
_backgroundColor: Cornsilk
_color:
_class:
    - lead
-->

### 用户感知偏差

---

#### 用户感知偏差的来源

1. 信息排序引入的偏差
1. 设置极限值（异常值）引入的偏差
1. 视觉感知引入的偏差

---

![height:600](./resources/image/fig11_04.jpg)

---

#### 避免用户感知偏差的方法

1. 定位来源
1. 用户参与设计
1. 用户参与测试
1. 构建灵活的输出
1. 培养用户习惯

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 根据媒介设计输出

---

<!--
_backgroundColor: Cornsilk
_color:
_class:
    - lead
-->

### 设计报表打印输出

---

1. 详细记录：考虑屏幕输出代替大部分的详细记录打印输出
1. 异常报表
1. 概要报表/统计报表

---

<!--
_backgroundColor: Cornsilk
_color:
_class:
    - lead
-->

### 设计屏幕输出

---

#### 指导原则

1. 保持显示的简单性
1. 保持显示的一致性
1. 方便用户在屏幕输出间切换
1. 创建有吸引力的画面

---

![height:660](./resources/image/fig11_05.jpg)

---

![height:660](./resources/image/fig11_06.jpg)

---

#### 图形

![height:580](./resources/image/fig11_07.jpg)

---

#### 仪表盘

---

![height:660](./resources/image/fig11_08.jpg)

---

![width:1100](./resources/image/GA-dash-v2-1024x539.jpg)

---

### 信息图

![height:600](./resources/image/fig11_09.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 设计web输出

---

1. 响应式web设计（Responsive Web Design, RWD）：也称为“自适应网页设计”，根据用户行为以及设备环境（系统平台、屏幕尺寸、屏幕定向等）进行相应的响应和调整
1. 扁平化web设计（Flat Web Design, FWD）：一种简约的UI设计理念，强调对于三维效果图像元素使用的最小化（阴影、渐变和纹理）

>+ [响应式网页设计.百度百科](https://baike.baidu.com/item/%E5%93%8D%E5%BA%94%E5%BC%8F%E7%BD%91%E9%A1%B5%E8%AE%BE%E8%AE%A1)
>+ [响应式网页设计.wikipedia](https://zh.wikipedia.org/zh-cn/%E5%93%8D%E5%BA%94%E5%BC%8F%E7%BD%91%E9%A1%B5%E8%AE%BE%E8%AE%A1)

---

1. web2.0技术：用户生产信息
1. ajax(Asynchronous Javascript And XML)：页面局部更新
1. 单页面设计

>当你的设计是为了追随时尚潮流，而不是为了满足用户的目标为目的，那你的设计就出现问题了。  
>[网站设计：单页还是多页好？](http://www.woshipm.com/ucd/636844.html)

---

![width:1100](./resources/image/fig11_10.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 设计app输出

---

### Logo设计

---

![height:600](./resources/image/fig11_13.jpg)

---

![height:600](./resources/image/fig11_14.jpg)

---

![height:600](./resources/image/mi_logo.jpg)

---

### 设计适用于设备的输出

---

![height:600](./resources/image/fig11_15.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 设计社交媒体应用的输出

---

1. 强调设计目标
1. 强调一致外观
1. 强调用户的视觉关注点
1. 合理布局使用空间

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
