---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Designing Databases_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 数据库设计Designing Databases

---

## Contents

1. 数据库与数据库的基本概念
1. 关系型数据库
1. 关系型数据库的规范化与反规范化
1. 关系型数据库的设计准则
1. 数据仓库、数据分析、商业智能
1. 区块链

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据库与数据库的基本概念

---

+ 数据库系统 = 数据库管理员 + 数据库管理系统 + 数据库
+ 数据库是储存在计算机中的、有组织的、可共享的数据集合
+ 数据库中的数据存储在数据库管理系统管理的文件中

---

### 数据库系统的目标

1. 确保数据能被授权的应用程序和用户共享
1. 维护数据的准确性和一致性/完整性
1. 允许随需求的变更而不断演变
1. 允许用户建立数据视图，而不用关心数据的实际存储方式

---

### 实体、记录和元数据

1. 实体entity：物理世界的物件或事件
    1. 属性attribute：实体的特征
    1. 关系relationship：实体之间的联系
1. 记录record：计算机世界用于表征实体或关系的一组数据项
    1. 数据项data item/字段field
    1. 键key：可以标识一个记录的一个或多个数据项
1. 元数据metadata：用于描述数据的数据

---

![width:1100](./resources/image/fig13_01.jpg)

---

![height:600](./resources/image/fig13_03.jpg)

---

![height:600](./resources/image/fig13_05.jpg)

---

![width:1100](./resources/image/fig13_06.jpg)

---

![height:600](./resources/image/fig13_07.jpg)

---

### 数据库的文件

+ 长时文件：
    1. 主文件
    1. 表文件
+ 临时文件：
    1. 事务文件
    1. 报表文件

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 关系型数据库

---

+ 关系型数据库建立在关系代数之上
+ 关系型数据库的简史
    1. 1970：F. Codd, E. (1970). A Relational Model of Data for Large Shared Data Banks. Commun. ACM. 13. 377-387. 10.1007/978-3-642-48354-7_4.
    1. Don Chamberlin创建`SQL`
    1. Larry Elision发布第一个商用大型关系型数据库`Oracle`

---

![height:600](./resources/image/fig13_08.jpg)

---

### 数据库系统的发展史

1. 无数据库
1. 层次型数据库
1. 网状型数据库
1. 关系型数据库
1. `NoSQL`
    1. `non-SQL`
    1. `Not Only SQL`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 关系型数据库的规范化与反规范化

---

+ 数据库规范化是根据一系列所谓的规范形式重组关系数据库的过程，目的是 **_减少数据冗余_** 和 **_提高维护数据完整性的效率和效果_**
+ 范式Normal Form
    1. `1NF`：列不重复且原子
    1. `2NF`：增加主键/非键属性完全依赖于主键/非键属性由主键决定
    1. `3NF`：不存在传递依赖（非键属性依赖于其他的非键属性）/非键属性相互独立
+ 反规范化：增加冗余/降低范式以提升性能

---

![height:600](./resources/image/fig13_10.jpg)

---

><https://cloud.tencent.com/developer/article/1774588>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 关系型数据库的设计准则

---

1. 为每个独立的实体创建一张关系表
1. 除主键或索引字段外的字段应只出现在一个关系表中
1. 每张关系表对应`CRUD`代码，理想的情况下，只由一段代码负责`CRU`操作
1. 保证完整性约束：实体完整性、域完整性、引用完整性
1. 避免数据异常：数据异常（冗余不一致）、插入异常、删除异常、更新异常

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 数据仓库、数据分析、数据挖掘、商业智能

---

+ 数据库系统：`OLTP(On-Line Transaction Process) application`
+ 数据仓库系统：`OLAP(On-Line Analytic Process) application`
    1. 通常是非规范化模式
    1. 通常围绕主题组织数据
    1. 存储一个实体的不同维度的数据
    1. 满足决策需要

---

+ 数据分析：对数据进行分析
+ 数据挖掘：从数据中挖掘出未知的模式/规则/知识（相关性、因果性等）的一种应用
+ 商业智能：一类由数据仓库（或信息市集）、查询报表、资料分析、数据挖掘、资料备份和恢复等部分组成的、以帮助企业决策为目的技术及其应用

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 区块链Blockchain

---

+ 区块链网络是建立在互联网之上的分布式数据库
+ 区块链网络是允许创建和共享数据的数字账簿
+ 区块链的数据可以在公共或私人网络上与其他人分享
+ 区块链的数据不需要中介（例如：数字货币的商业银行）即可进行交易

---

### 区块链的优势和应用

+ 区块链的最大优势之一是不可篡改，是一个开放的、不可变的交易记录
+ 区块链的应用除了加密货币外，还有跟踪溯源等

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
