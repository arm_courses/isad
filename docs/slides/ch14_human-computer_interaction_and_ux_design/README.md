---
marp: true
theme : gaia
class : invert + lead
size: 16:9
auto-scaling: true
paginate: false
color: white
backgroundColor: #202228
header: "_Human-Computer Interaction and UX Design_"
footer: "@aRoming"
---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# 人机交互与UX设计Human-Computer Interaction and UX Design

---

## Contents

1. `HCI`人机交互
1. `UI`用户界面
1. `UX`用户体验
1. 一些具体的`UX`设计

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 人机交互Human-Computer Interaction

---

+ `HCI设计`：指确保系统的功能和可用性，提供有效的用户交互的支持、**_增强用户的快乐体验_**
+ `HCI设计`总的目标是实现组织和个人用户的效率和有效性，为达到这些目标，管理者和开发都需要知道 **_用户_**、**_任务_**、**_任务环境_**、**_信息技术_** 和 **_系统使用环境_** 之间的相互影响
+ `HCI设计`的主要策略是：不断引出用户对原型设计的使用反馈，根据用户反馈改进，如此循环，直至用户满意为止

---

+ `HCI`的元素
    1. 人
    1. 计算机
    1. 待执行的任务
+ `HCI`元素之间的有效配合产生绩效和幸福感
    1. 绩效：指执行一个任务的效率和该任务产生的工作的质量
    1. 幸福感：人的身体和心理状态，例如：人的整体的舒适、安全和健康等

---

### 为个体用户设计多样化的风格

+ 数据（特别是用于决策的数据）应以不同的形式存在，使具有不同认知能力或喜好的用户都可以接受
+ 数据可视化：将数据表示为图表、图形或其他图像

---

![width:1100](./resources/image/data_visualization.jpg)

---

### HCI设计中的物理因素 -- 视觉

+ 字体、色彩、图形等
+ 视觉缺陷问题
+ 视觉规范

>[视觉设计规范那些事](https://www.zcool.com.cn/article/ZNTE1ODQw.html)

---

### 字体的类型

1. 衬线字体
1. 非衬线字体
1. 手写体

><https://zhuanlan.zhihu.com/p/28569633>

---

![width:1100](./resources/image/fonttype_01.png)

---

![width:1100](./resources/image/fonttype_02.png)

---

![width:1100](./resources/image/fonttype_03.png)

---

![width:1100](./resources/image/fonttype_04.png)

---

![width:1100](./resources/image/fonttype_05.png)

---

### HCI设计中的物理因素 -- 听觉

+ 90分贝最大耐受8小时；115分贝最大耐受15分钟
+ 个体的听觉压力承受能力
+ 噪声标准：

>[噪声知多少 听觉保护好](http://wsjkw.sh.gov.cn/zyfhyzybfz/20180525/0012-30543.html)

---

### HCI设计中的物理因素 -- 触觉

+ 人机配合程度
+ 使用习惯

>[QWERTY Keyboard](https://en.wikipedia.org/wiki/QWERTY)

---

### 考虑人的缺陷、残障而加以设计

+ 所有人都存在一定的物理局限性
+ 视觉障碍、听觉障碍、行动障碍

>[如何建立标准化听觉](https://zhuanlan.zhihu.com/p/31233639)

---

### HCI的一些原则

1. 准确地、意义明确地交互
1. 最小化用户操作
1. 操作的标准化和一致性

---

### HCI设计的流程指导

1. 分析任务、考虑人、计算机和任务之间的配合
1. 识别用户完成任务时可能存在的障碍
1. 谨记技术的感知有用性和感知易用性
1. 考虑有用性
1. 确定物理环境和组织环境特征，通过原型法适应各种不同的用户（包括残障用户）

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## UI用户界面

---

+ 对于大多数用户来说，界面就是系统
+ 界面设计的优劣反映系统设计的水平、也反映系统设计人员是否胜任本职工作

---

### UI设计的目标

1. 界面与任务相匹配
1. 提高界面的效率
1. 提供适当的反馈
1. 生成有用的结果
1. 提高用户的效率

---

### UI的类型

1. 语音界面：自然语言
1. 视觉界面：
    1. `CLI`
    1. `GUI`

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## UX用户体验

---

+ `UX设计`是一种用户至上的设计方法，观察用户行为、努力提高用户满意度和忠诚度
+ `UX设计`通过提高可用性和易用性实现目标
+ `UX`“好”的表现：超出期望的那部分感觉

---

### UI vs UX

+ `UI`考虑设计流畅的交互，重点是布局、菜单、输入按钮，以及办理出的图形、图表和表格
+ `UX`面向用户的要求和需求，以人为中心的流程，最重要的是可用性

---

1. `UI`是使用的那部分，`UX`是在使用时的感受
1. `UI`职责是布局设计，视觉设计，品牌形象，`UX`职责是用户画像、用户故事、用户调研、可用性测试
1. `UI`涉及人机交互、工业设计、视觉和声音设计等，`UX`包含信息架构、内容策略在内的更多部分，需要从整体动态流程上考虑产品是否能解决用户的问题。`UI`属于`UX`的一部分。

>1. [10 张图秒懂UI和UX到底有何不同？](https://www.uisdc.com/ui-and-ux-differences)
>1. [UI 设计和 UX 设计的区别是什么？](https://www.zhihu.com/question/19567997)

---

![width:1100](./resources/image/ui_vs_ux_01.jpg)

---

![width:1100](./resources/image/ui_vs_ux_02.png)

---

### UI vs HCI vs UX

>1. [交互设计师到底是干嘛的？](https://www.uisdc.com/what-is-interaction-designer)
>1. [人机交互百科全书：这可能是世界上最全面的设计知识纲要](https://zhuanlan.zhihu.com/p/77326457)
>1. [什么是用户界面和体验设计](https://zhuanlan.zhihu.com/p/36813805)
>1. [UI 设计进阶 1-1：带你全面认识 UI、UX、IxD、HCI 等术语](https://zhuanlan.zhihu.com/p/34042294)

---

![height:600](./resources/image/ui_vs_hci_vs_ux.png)

---

![width:1100](./resources/image/pd_ui_hci_dev.jpg)

---

![height:600](./resources/image/fig14_04.jpg)

---

+ 可以依据产品是否能按预期方式使用来判断质量，这是`UX设计`的核心
+ 可用性意味着系统易于理解、易于使用且容易被接受

---

### UX设计应考虑的问题

1. 是否满足了用户的需求？不存在任何困难？
1. 工作流程是否继续而不需要额外或重复的步骤？
1. 用户是否不必被迫回忆过去的经验即可完成任务？
1. 如果用户犯了错误，是否能轻松地纠正？
1. 交互对于用户来说是否自然？
1. 用户使用快捷方式是否能提高效率？
1. 用户是否感觉在控制系统？而不是系统在控制自己？

:point_right: `UX设计师`需要研究用户群的心理特征 :point_left:

---

### 电商UX设计准则

1. 尽早解释运费并使其易于发现
1. 动态更新价格并向用户提供注释
1. 允许用户轻松更新商品数量
1. 清晰地标明信用卡费用和交易费用
1. 将商品加入购物车后，允许用户继续购物或立即结账
1. 结账付款前，提醒用户查看交易详情

---

### UX设计的益处

1. 用户更快完成任务、完成更多任务
1. 任务结果更准确
1. 减少人力用于服务支持
1. 用户忠诚度高

---

![width:1100](./resources/image/fig14_05.jpg)

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 智能手机和平板电脑的UX设计

---

### 手势

+ 一些手势生来就有
+ 一些手势一学即有
+ 一些手热习惯即有

---

### 告警、通知和询问

1. 告警alert：将 **_用户需要_** **_及时_** 了解的 **_关键信息_** 通知给用户
1. 通知notify：将 **_用户需要_** 了解的一些信息 通知用户
1. 询问query：询问用户一些 **_系统需要_** 了解的信息

---

### 标记Badges

![height:200](./resources/image/badges.png)

>[交互漫谈：详解未读消息时的红点提示](http://www.woshipm.com/pd/634813.html): <http://www.woshipm.com/pd/634813.html>

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

## 一些具体的UX设计

---

### 智能个人助理Intelligent Personal Assistants

+ 智能个人助理：可以接受用户编写或说出的命令并基于该输入执行任务的软件代理
+ `Apple Siri`/`SiriKit`, `Google Assistant`, `Amazon Alexa`, `Microsoft Cortana`

---

![width:1100](./resources/image/fig14_06.jpg)

---

### 虚拟现实和增强现实Virtual Reality and Augmented Reality

+ `VR`：人造的、由计算机生成的世界
+ `AR`：人工元素与现实世界相结合

---

![height:600](./resources/image/fig14_07.jpg)

---

### 反馈

+ 人在执行行动过程中，大多希望能得到有效的反馈
+ 反馈的类型
    1. 反馈已接收到输入的数据
    1. 反馈输入的格式正确
    1. 反馈输入的格式不正确
    1. 反馈当前处理延迟
    1. 反馈用户请求已完成
    1. 反馈用户请求未完成
    1. 提供更详细的反馈信息

---

![height:600](./resources/image/fig14_09.jpg)

---

### 电子商务的特殊设计因素

+ 获取客户的反馈
+ 简单有效的网站导航

---

### Mashup

+ `mashup`：指整合网络上多个资料来源或功能，以创造新服务的网络应用程序

>[混搭 (互联网)](https://zh.wikipedia.org/zh-cn/%E6%B7%B7%E6%90%AD_(%E4%BA%92%E8%81%AF%E7%B6%B2))

---

<!--
_backgroundColor: FloralWhite
_color:
_class:
    - lead
-->

# Thank You

## :ok: End of This Slide :ok:
