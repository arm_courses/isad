# Review Question

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=2 orderedList=true} -->

<!-- code_chunk_output -->

1. [Chapter 01](#chapter-01)
2. [Chapter 02](#chapter-02)
3. [Chapter 03](#chapter-03)
4. [Chapter 04](#chapter-04)
5. [Chapter 05](#chapter-05)
6. [Chapter 06](#chapter-06)
7. [Chapter 07](#chapter-07)
8. [Chapter 10](#chapter-10)
9. [Chapter 11](#chapter-11)
10. [Chapter 12](#chapter-12)
11. [Chapter 13](#chapter-13)
12. [Chapter 14](#chapter-14)

<!-- /code_chunk_output -->

## Chapter 01

### page 17 review question 6: What are CASE tools used for?

`CASE(Computer-Aided Software Engineering, 计算机辅助软件工程)`是以计算机为工具，辅助人在特定应用领域内完成任务的理论、方法和技术，`CASE`可以用于提升软件工程的沟通效率、协作效率、开发效率和产品质量等。

## Chapter 02

### page 41 review question 10: What are enterprise resource planning (ERP) systems?

`ERP(Enterprise Resource Planning, 企业资源计划)`是一个将企业内外部资源全面整合的系统，用于辅助管理企业的人（人力）、财（财务）、物（物资生产）、供（供应链）、销（销售）、存（库存）的一套企业信息系统。

## Chapter 03

### page 87 review question 22: What is work breakdown structure (WBS), and when should it be used?

`WBS(Work Breakdown Structure, 工作任务分解)`是指将项目工作分解成更小的活动，这些更小的活动具有以下特征：

1. 可交付：每个活动包含一个可交付的或明确的成果
1. 可分派：每个活动可分配给一个人或一个小组
1. 可控制：每个活动有一个负责人负责监督和控制

当需要有效地执行和控制项目工作，达到成本、时间、质量、范围相均衡的情况时，需要使用`WBS`。

## Chapter 04

### page 118 question 1: What kinds of information should be sought in interviews?

面谈时，需要获取面谈对象的观点，以及他们对系统当前状态、组织与个人目标、非常规程序的感受。

### page 119 question 19: What are the two basic question types used on questionnaires?

1. 开放式（open-ended）：面谈对象对问题可以进行不受限制的答复
1. 封闭式（closed）：面谈对象对问题只能在提供可选择项中选择其中的一个或多个进行答复

## Chapter 05

### page 142 question 8: What is text analytics?

对非结构化的文本数据进行分析，以协助进行下一步的信息挖掘、信息检索等应用。

## Chapter 06

### page 174 question 10: What are the four core practices of the agile approach?

1. 快速发布/简短发布
1. 保证休息/每周工作40小时
1. 用户参与/长期驻留一个用户代表
1. 结对编程

### page 174 question 15: What is Scrum?

`Scrum`是敏捷方法的一种，`Scrum`的特征有：

+ `Scrum`实践团队工作，类似橄榄球赛的团队一样，团队成员认识到项目的成功最重要，个人成功是次要的
+ `Scrum`注重行动并持续改进，以一个概要计划开始项目并不断地根据进展改变，类似橄榄球赛会采取一个总体策略一样
+ `Scrum`有一个严格的时间框架`Sprint`（2~4周一个`Sprint`），类似橄榄球赛对比赛时间有严格限制一样

### page 174 question 24: What does DevOps stand for?

1. `DevOps`是一种文化：强调开发运维一体化、强调自动化、强调协作、强调持续改进
1. `DevOps`是一系列的方法及其相应的工具链：自动化、持续集成/持续交付/持续部署、持续监控、持续迭代

## Chapter 07

### page 203 question 2: What are the four advantages of using a data flow approach over narrative explanations of data movement?

:book: P180 & P203

使用数据流图的4个主要优势是：

1. 不需要过早地考虑系统的技术实现
1. 可以更深入地理解系统和子系统的相互关系
1. 可以更深入地与用户交流关于系统的信息
1. 可以帮助判断是否已定义必要的数据和过程

### page 204 question 5: Define the top-down approach as it relates to drawing data flow diagrams.

:book: P182

在绘制`DFD`时，自顶向下方法指的是先绘制系统的鸟瞰图，然后再为这个图添加细节，换句话说，指从一般到特殊的一种方法。

### page 204 question 9: What is the difference between a logical and physical data flow diagram?

:book: P187

逻辑数据流图关注业务及其运营，物理数据流图关注系统如何构建和使用。

## Chapter 10

### page 284 question 2: Describe the difference between a class and an object

1. 类是静态的，对象是动态的
1. 类是对象的模板，对象是类的实例

### page 285 question 6: What is UML?

`UML (Unified Modeling Language, 统一建模语言)`是一种由一整套图表组成的标准化的、用于系统分析与设计的、面向对象的建模语言，`UML`的一种重要特征是“用例驱动”。

### page 285 question 13: What does a use case model describe?

1. 用例模型包括用例图和用例场景，一张用例图包含多个用例，一个用例对应一个用例场景
1. 用例模型描述用户如何使用系统，即`actor`发出一个`event`，`event`触发一个`usecase`，`usecase`执行该`event`触发的行为
1. 用例模型关注系统做什么，而不是关注系统怎么做

### page 285 question 14: Would you describe a use case model as a logical or physical model of the system? Defend your answer in a paragraph.

用例模型是逻辑模型。用例模型描述系统做什么，而不是怎么做

### page 285 question 25: What are the two categories of relationships between classes?

类之间的六种关系是：泛化、实现、依赖、关联、聚合/聚集、组合，各种关系的强弱顺序：泛化 = 实现 > 组合 > 聚合 > 关联 > 依赖

|-|代码表现|语义|UML元素|
|--|--|--|--|
|泛化<br/>**`generalization`**|继承<br/>**`extends`**|is-a<br/>一般-特殊关系|实线、带空心三角箭头，箭头指向父类|
|实现<br/>**`realization`**|实现<br/>**`implements`**|be-able-to/has-capability<br/>接口-实现关系|虚线、带空心三角箭头，箭头指向接口|
|依赖<br/>**`dependency`**|局部变量（含形参）|use-a<br/>使用与被使用关系|虚线、带普通箭头，箭头指向被使用者|
|关联<br/>**`association`**|成员变量|has-a<br/>平等拥有关系（可双向拥有）|实线、带普通箭头，箭头指向被拥有者|
|聚合<br/>**`aggregation`**|成员变量|contains-a<br/>整体-部分关系（部分可独立存在）|实线、带空心菱形，菱形指向整体|
|组合<br/>**`composition`**|成员变量|owns-a<br/>整体-部分关系（部分不可独立存在）|实线、带实心菱形，菱形指向整体|

>[六大类UML类图关系](https://segmentfault.com/a/1190000021317534)

### page 285 question 28: What does a state diagram depict?

状态图对象的可能存在的状态，以及由事件导致这些状态之间迁移的路径。

## Chapter 11

### page 322 question 20: Define the term "responsive web design".

:book: P304 & P322

响应式web设计也称为“自适应网页设计”，根据用户行为以及设备环境（系统平台、屏幕尺寸、屏幕定向等）进行相应的响应和调整，响应式web设计的表现为网站可以在任何设备上浏览，如台式机、笔记本电脑、平板电脑或智能手机。

### page 323 question 40: What are the main uses for dashboards?

:book: P302

仪表盘的主要用途是：向用户传达测量值，主管利用仪表盘查看各项指标，并根据屏幕上显示的信息采取措施（如果有必要的话）

### page 323 question 44: How does Ajax help to build effective Web pages?

:book: P321

ajax技术通过javascript向服务器请求并处理响应实现异步传输，从而达到局部更新页面的效果。

## Chapter 12

### page 347 question 14: When should check boxes be used?

:book: P337

当用户可以在多个选项中选择一个或多个选项时使用复选框。

### page 347 question 15: When should option buttons be used?

:book: P337

当用户只能在多个选项中选择一个选项时使用单选框。

### page 347 question 24: Describe how a hamburger menu or icon functions on a Web page.

:book: P344

汉堡菜单/汉堡图标通常放在显示的顶部，提示用户可以点击图标以显示菜单，特别适合应用于移动设备

### page 347 question 25: How does context-sensitive help differ from online help?

:book: P344

上下文相关帮助一般直接从本网页中获取帮助信息并显示出来，在线帮助一般链接到另一个网址。

### page 347 question 27: What is a breadcrumb trail in Web design?

:book: P344，<https://zh.wikipedia.org/zh-cn/%E9%9D%A2%E5%8C%85%E5%B1%91%E5%AF%BC%E8%88%AA>

面包屑导航通常在页面顶部水平出现，一般会位于标题或页头的下方。它们提供给用户返回之前任何一个页面的链接（这些链接也是能到达当前页面的路径），在层级架构中通常是这个页面的父级页面。页面路径提供给用户回溯到网站首页或入口页面的一条路径，通常是以大于号（>）出现。

例如：`首页>分类页>次级分类页` 或 `首页>>分类页>>次级分类页`。

## Chapter 13

### page 386 question 1: What are the advantages of organizing data storage as separate files?

使用文件系统存储数据的优势有：

1. 可以为该应用优化性能（或优化特定的某个需求）
1. 可以去除数据库系统中该应用所不需要的额外功能
1. 可以消除对数据库系统的依赖

### page 386 question 2: What are the advantages of organizing data storage using a database approach?

:book: P353

使用数据库系统存储数据的优势有：

1. 有利于数据共享（数据库管理系统提供了并发访问控制的功能）
1. 有利于数据安全（数据库管理系统或其他第三方兼容系统提供了安全功能）
1. 有利于数据规范化
1. 有利于快速开发（数据库系统提供了开箱即用的数据相关功能）
1. 有利于迭代升级（大多数关系型数据库系统都支持`SQL`标准）

### page 386 question 7: Define the term “metadata.” What is the purpose of metadata?

:book: P358

元数据是关于数据的数据。元数据的目的是提供关于数据的构成和结构的信息，以便用户或程序有效地使用数据。

## Chapter 14

### page 420 question 25: Why is it a good idea to avoid the use of a badge on apps?

:book: P403

因为有时`badge`容易被用户忽略，且有时信息容易过时。

### page 420 question 34: Define what is meant by UX design.

:book: P397

`UX设计`是一种用户至上的设计方法，`UX设计`注重观察用户行为，并努力提高用户满意度和忠诚度。`UX设计`通过提高产品的可用性和易用性来实现这一目标，也包括提升用户在与产品交互时的愉悦感。

### page 420 question 35: In a sentence, compare and contrast the differences between taking a traditional approach to design versus adopting a UX design approach

:book: P398

`传统设计方法`，设计人员从高层管理层获取战略目标，然后设计信息系统，再将该设计结果推送给用户；`UX设计方法`，设计人员从用户那里收集的需求，然后与管理人员共享用户目标，然后设计信息系统，再将该设计结果提供给用户。
